DROP DATABASE IF EXISTS `nodedb_test`;
CREATE DATABASE `nodedb_test`;
USE `nodedb_test`;

CREATE USER 'nodeDB_test_user'@'%' IDENTIFIED BY 'secret';
CREATE USER 'nodeDB_test_user'@'localhost' IDENTIFIED BY 'secret';

GRANT ALL ON `nodedb_test`.* TO 'nodeDB_test_user'@'%';
GRANT ALL ON `nodedb_test`.* TO 'nodeDB_test_user'@'localhost';

CREATE TABLE `users` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(32) NOT NULL,
  `Lastname` varchar(64) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Residence` varchar(128) NOT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `Lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;


CREATE TABLE `attendees` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ActivityId` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


CREATE TABLE `activity` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(32) NOT NULL,
  `Description` varchar(32) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `OwnerID` int(10) NOT NULL,
  `LaatstGewijzigdOp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

