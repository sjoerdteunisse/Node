const chai = require('chai');
const server = require('../server');
const pool = require('../src/config/mySql');

const chaiHttp = require('chai-http');
chai.should();
chai.use(chaiHttp);

require('./authentication.test');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const baseEndpointToTest = '/api/activities';
let finalToken = "";
let finalToken2 = "";

describe('Prerequisite', () => {
    //As module export doesn't work, we have gone for the prerequisite setup for each Unit test Run.

    it('account 1 - should return a valid token from Authorization controller test to start the test sequence', (done) => {
        const user = {
            email: 'user@server.com',
            password: 'secret'
        }

        bcrypt.hash(user.password, saltRounds, (err, hash) => {
            chai.request(server)
                .post('/api/user/login')
                .send(user)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('token').that.is.a('string');
                    finalToken = res.body.token;
                    done();
                });
        });
    });

    it('account 2 - should return a valid token from Authorization controller test to start the test sequence', (done) => {
        const user = {
            email: "SuperSecure2@gmail.com",
            password: "VerrySecure"
        }
        bcrypt.hash(user.password, saltRounds, (err, hash) => {
            chai.request(server)
                .post('/api/user/login')
                .send(user)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('token').that.is.a('string');
                    finalToken2 = res.body.token;
                    done();
                });
        });
    });

    it('account 1 - tests if ME can be called after authentication to retrieve the users identity', (done) => {

        chai.request(server)
            .get('/api/user/me')
            .set('x-access-token', finalToken)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.have.property('id');
                res.body.should.have.property('email');
                done();
            });

    });

    it('account 2 - tests if ME can be called after authentication to retrieve the users identity', (done) => {

        chai.request(server)
            .get('/api/user/me')
            .set('x-access-token', finalToken2)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.have.property('id');
                res.body.should.have.property('email');
                done();
            });

    });
});

//Api POST test.
describe('Activity API POST', () => {
    it('Activity should post an Activity', (done) => {

        chai.request(server)
            .post(baseEndpointToTest)
            .set('x-access-token', finalToken)
            .send({
                'name': 'Watching',
                'description': 'New video',
                'startdatetime': '2019-01-11 14:00',
                'enddatetime': '2019-01-11 14:15',
                'maxAttendees': '10'
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')

                //Get object from body the response.
                const Activity = res.body;

                //Check if properties are still existent in object returned.
                Activity.should.have.property('name');
                Activity.should.have.property('description');
                Activity.should.have.property('startdatetime');
                Activity.should.have.property('enddatetime');
                Activity.should.have.property('maxattendees');
                Activity.should.have.property('ownerId');


                //Do the properties still match?
                Activity.name.should.equal('Watching');
                Activity.description.should.equal('New video');
                Activity.startdatetime.should.equal('2019-01-11 14:00');
                Activity.enddatetime.should.equal('2019-01-11 14:15');
                Activity.maxattendees.should.equal('10');

                done();
            });
    });

    it('Activity should throw an error, that endpoint cannot add an invalid object', (done) => {

        chai.request(server)
            .post(baseEndpointToTest)
            .set('x-access-token', finalToken)
            .send({
                'name': 'Watching',
                'description': 'New video',
                'startdatetime': '2019-01-11 14:00',
                'maxAttendees': '10'
            })
            .end((err, res) => {
                res.should.have.status(422)
                res.body.should.be.a('object')

                //Get object from body the response.
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Failed to add object');

                done();
            });
    });

    it('Activity should throw an error, that endpoint cannot add an invalid object 2', (done) => {

        chai.request(server)
            .post(baseEndpointToTest)
            .set('x-access-token', finalToken)
            .send({
                'name': 'Watching',
                'startdatetime': '2019-01-11 14:00',
                'maxAttendees': '10'
            })
            .end((err, res) => {
                res.should.have.status(422)
                res.body.should.be.a('object')

                //Get object from body the response.
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Failed to add object');

                done();
            });
    });
});

//Api GET test.
describe('Activity API GET', (done) => {
    it('GetAll should return an array of activities', (done) => {
        chai.request(server)
            .get(baseEndpointToTest)
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('array')
                res.body.should.have.length(1)
                done();
            });
    });

    it('GetById should return an Acitivity at position 1', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')
                const Activity = res.body;

                //Check if properties are still existent in object returned.
                Activity.should.have.property('name');
                Activity.should.have.property('description');
                Activity.should.have.property('startdatetime');
                Activity.should.have.property('enddatetime');
                Activity.should.have.property('maxattendees');
                Activity.should.have.property('ownerId');


                //Do the properties still match?
                Activity.name.should.equal('Watching');
                Activity.description.should.equal('New video');
                //Explicit time checking not done here. as MySql parses to ISO-8601
                //Client should parse to DateTime, and use DateTime.GETUTCHOURS(), GETUTSMMINUTES(), GETUTCSECONDS
                Activity.maxattendees.should.equal(10);

                done();
            });
    });

    it('GetById should return an Error as an undefined/not existing object throws a error', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + '/99')
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.a('object');

                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Object not found');
                done();
            });
    });
});

//Api PUT test.
describe('Activity API PUT', (done) => {
    it('PutById should replace an objects values', (done) => {
        chai.request(server)
            .put(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken)
            .send({
                'name': 'Temp',
                'description': 'Test',
                'startdatetime': '2019-01-11 12:00',
                'enddatetime': '2019-01-11 13:15',
                'maxAttendees': '25'
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')

                const Activity = res.body;

                //Check if properties are still existent in object returned.
                Activity.should.have.property('name');
                Activity.should.have.property('description');
                Activity.should.have.property('startdatetime');
                Activity.should.have.property('enddatetime');
                Activity.should.have.property('maxattendees');
                Activity.should.have.property('ownerId');

                //Check if values match the expected return value.
                Activity.name.should.equal('Temp');
                Activity.description.should.equal('Test');
                Activity.startdatetime.should.equal('2019-01-11 12:00');
                Activity.enddatetime.should.equal('2019-01-11 13:15');
                Activity.maxattendees.should.equal('25');

                done();
            });
    });

    it('PutById should throw an error that object cannot be replaced as its non exsistent', (done) => {
        chai.request(server)
            .put(baseEndpointToTest + '/99')
            .set('x-access-token', finalToken)
            .send({
                'name': 'Temp',
                'description': 'Test',
                'startdatetime': '2019-01-11 12:00',
                'enddatetime': '2019-01-11 13:15',
                'maxAttendees': '25'
            })
            .end((err, res) => {
                res.body.should.be.a('object');

                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Object not found');

                done();
            });
    });

    it('PutById should throw an error on an object that belongs to someone else', (done) => {
        chai.request(server)
            .put(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken2)
            .send({
                'name': 'Temp',
                'description': 'Test',
                'startdatetime': '2019-01-11 12:00',
                'enddatetime': '2019-01-11 13:15',
                'maxAttendees': '25'
            })
            .end((err, res) => {
                res.body.should.be.a('object');

                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('You are not the rightfull owner, not allowed to modify.');

                done();
            });
    });
});

//Api Attendee POST test.
describe('Attendee API POST', () => {
    it('Should post an attendee', (done) => {
        chai.request(server)
            .post(baseEndpointToTest + "/1" + "/participants")
            .set('x-access-token', finalToken)
            .send()
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                done();
            });
    });

    it('Should should throw an error when posting on a non existent activity', (done) => {
        chai.request(server)
            .post(baseEndpointToTest + "/200" + "/participants")
            .set('x-access-token', finalToken)
            .send()
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.a('object');
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Object not found');
                done();
            });
    });
});

//Api Attendee GET test.
describe('Attendee API GetAll Attendees', (done) => {

    it('GetAll - should return an array of activities', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + "/1" + "/participants")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('array')
                res.body.should.have.length(1)
                done();
            });
    });

    it('GetAll - Should should throw an error when retrieving a non existing object', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + "/200" + "/participants")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.a('object');
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Object not found');
                done();
            });
    });
});

//Api Attendee GET test.
describe('Attendee API GetById Attendees', (done) => {
    it('GetById - should return an Attendee', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + "/1" + "/participants/0")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('object')

                const Attendee = res.body;

                //Check if properties are still existent in object returned.
                Attendee.should.have.property('id');
                Attendee.should.have.property('firstname');
                Attendee.should.have.property('lastname');
                Attendee.should.have.property('email');
                Attendee.should.have.property('residence');

                done();
            });
    });

    it('GetById - should return an error that the participant doesnt exist', (done) => {
        chai.request(server)
            .get(baseEndpointToTest + "/1" + "/participants/120")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(404)
                done();
            });
    });
});


//Api Attendee DELETE test.
describe('Attendee API Delete Attendees', (done) => {
    
    it('AttendeeDelete - With by owner and valid token ', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + "/1" + "/participants/1")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('object');

                const response = res.body;

                //Check if properties are still existent in object returned.
                response.should.have.property('message');

                //Do the properties still match?
                response.message.should.equal('Succesfully removed');
                done();
            });
    });

    it('AttendeeDelete - With an unauthorized owner token', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + "/1" + "/participants/1")
            .set('x-access-token', finalToken2)
            .end((err, res) => {
                res.should.have.status(401);
                res.body.should.be.an('object')
                done();
            });
    });

    it('AttendeeDelete - should return an error that the participant doesnt exist', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + "/1" + "/participants/120")
            .set('x-access-token', finalToken)
            .end((err, res) => {
                res.should.have.status(401)
                done();
            });
    });
});


//Api Activity DELETE test.
describe('Activity API DELETE', (done) => {

    it('DeleteById - should throw an error on an object that belongs to someone else', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken2)
            .send()
            .end((err, res) => {
                res.body.should.be.a('object');

                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('You are not the rightfull owner, not allowed to modify.');

                done();
            });
    });

    it('DeleteById - should return status 200 and the message succesfully removed ', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken)
            .send()
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')

                const response = res.body;

                //Check if properties are still existent in object returned.
                response.should.have.property('message');

                //Do the properties still match?
                response.message.should.equal('Succesfully removed');

                done();
            });
    });

    it('DeleteById - should throw an error that object cannot be replaced as its non exsistent ', (done) => {
        chai.request(server)
            .del(baseEndpointToTest + '/1')
            .set('x-access-token', finalToken)
            .send()
            .end((err, res) => {
                res.body.should.be.a('object');

                //Construct body
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Object not found');

                done();
            });
    });
});

//Invalid routing test.
describe('Calling an invalid route or failed call, should return an object of type ApiError', () => {

    it('should return a 404 error.', (done) => {
        chai.request(server)
            .get('/api/activitiesss')
            .send()
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.a('object');

                //construct body
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Non existing endpoint');

                done();
            })
    });

    it('should return a 404 error.', (done) => {

        chai.request(server)
            .get('/api/testttest')
            .send()
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.a('object');

                //construct body
                const apiError = res.body;

                //Check if properties are still existent in object returned.
                apiError.should.have.property('errorName');
                apiError.should.have.property('errorStatus');
                apiError.should.have.property('timeStamp');

                //Check if values match the expected return value.
                apiError.errorName.should.equal('Non existing endpoint');

                done();
            })
    });
});


after((done) => {
    const query = 'DELETE FROM `users`'
    const cd = 'DELETE FROM `activities`';
    const cdAttendee = 'DELETE FROM `attendees`';
    pool.query(cdAttendee, (err, rows, fields) => {
        if (err) {
            console.dir(err);
            assert.fail(err)
        }

        pool.query(cd, (err, rows, fields) => {
            if (err) {
                console.dir(err);
                assert.fail(err)
            }
            var queryReset = 'ALTER TABLE activities AUTO_INCREMENT = 1';
            pool.query(queryReset, (err, rows, fields) => {
                if (err) {
                    console.dir(err);
                    assert.fail(err)
                }

                pool.query(query, (err, rows, fields) => {
                    if (err) {
                        console.dir(err);
                        assert.fail(err)
                    } else {
                        var alterCount = 'ALTER TABLE users AUTO_INCREMENT = 1';
                        pool.query(alterCount, (err, rows, fields) => {
                            if (err) {
                                console.dir(err);
                                assert.fail(err)
                            }
                            var alterCountAttendee = 'ALTER TABLE attendees AUTO_INCREMENT = 1';
                            pool.query(alterCountAttendee, (err, rows, fields) => {
                                if (err) {
                                    console.dir(err);
                                    assert.fail(err)
                                }
                                done();
                            });
                        });
                    }
                })
            });
        });
    });
});