const chai = require('chai')
const assert = require('assert')
const Attendee = require('../src/models/attendee.model')


chai.should();


describe('Attendee', () => {

    it('Should initialize succesfully', (done) => {

        const attendee = new Attendee(0, 'sjoerd', 'teunisse', 'sjoerdteunisse@gmail.com', 'breda');
        attendee.should.have.property('firstname').that.is.a('string').and.equals('sjoerd');
        attendee.firstname.should.be.a('string');
        attendee.should.not.have.property('password');
    
        assert.equal(attendee.firstname ,'sjoerd', 'Names do not match');
    
        done();
    });
});