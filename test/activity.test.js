const chai = require('chai')
const assert = require('assert')
const acitivity = require('../src/models/activity.model')


chai.should();


describe('Activities', () => {

    it('Should initialize succesfully', (done) => {

        const activity = new acitivity(0, 'abc', 'description', '12:00:00 10-01-2019', '12:00:00 10-01-2019', 10, 1);
        activity.should.have.property('name').that.is.a('string').and.equals('abc');
        activity.name.should.be.a('string');

        activity.should.have.property('startdatetime').equal('12:00:00 10-01-2019');
        activity.should.not.have.property('password');
    
        assert.equal(activity.name ,'abc', 'Names do not match');
    
        done();
    });
});