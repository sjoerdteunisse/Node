const chai = require('chai')
const assert = require('assert')
const User = require('../src/models/user.model')

chai.should();


describe('User', () => {

    it('Should initialize succesfully', (done) => {
        const user = new User('abc', 'email@mail.com', 'complexPassword', 'breda');
     
        user.should.have.property('fullName').that.is.a('string').and.equals('abc');
        user.fullName.should.be.a('string');

        user.should.have.property('email').equal('email@mail.com');
        user.should.have.property('password');
    
        assert.equal(user.fullName ,'abc', 'Names do not match');
    
        done();
    });
});