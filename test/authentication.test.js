const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const pool = require('../src/config/mySql');

const bcrypt = require('bcrypt');
const saltRounds = 10;

chai.should();
chai.use(chaiHttp);

const endpointToTest = '/api/user';

const user = {
    firstname: 'firstname',
    lastname: 'lastname',
    email: 'user@server.com',
    residence: 'Breda',
    password: 'secret'
}


describe('Auth API', () => {

    before((done) => {
        // Verwijder alle voorgaande users uit de tabel
        const query = 'DELETE FROM `users`'
        const cd = 'DELETE FROM `activities`';
        const cdAttendee = 'DELETE FROM `attendees`';
        pool.query(cdAttendee, (err, rows, fields) => {
            if (err) {
                console.dir(err);
                assert.fail(err)
            }
           
            pool.query(cd, (err, rows, fields) => {
                if (err) {
                    console.dir(err);
                    assert.fail(err)
                }
                var queryReset = 'ALTER TABLE activities AUTO_INCREMENT = 1';
                pool.query(queryReset, (err, rows, fields) => {
                    if (err) {
                        console.dir(err);
                        assert.fail(err)
                    }
                    bcrypt.hash(user.password, saltRounds, (err, hash) => {

                        pool.query(query, (err, rows, fields) => {
                            if (err) {
                                console.dir(err);
                                assert.fail(err)
                            } else {
                                var alterCount = 'ALTER TABLE users AUTO_INCREMENT = 1';
                                pool.query(alterCount, (err, rows, fields) => {
                                    if (err) {
                                        console.dir(err);
                                        assert.fail(err)
                                    }
                                    // Registreer een user in de testdatabase
                                    const query = 'INSERT INTO `users` (`Firstname`, `Lastname`, `Email`,`Residence` , `Password`) VALUES (?, ?, ?, ?, ?)'

                                    const values = [user.firstname, user.lastname, user.email, user.residence, hash]

                                    pool.query(query, values, (err, rows, fields) => {
                                        if (err) {
                                            assert.fail(err)
                                        } else {
                                            done();
                                        }
                                    });
                                });
                            }
                        })
                    });
                });
            });
        });
    });

    // beforeEach functie wordt aangeroepen voor iedere test.
    beforeEach(function () {
        console.log('beforeEach')
        // set things we need for testing
        //
        // Je zou hier kunnen kiezen om de database voor iedere test leeg te maken 
        // en opnieuw te vullen met enkele waarden, zodat je weet wat er in zit.
        //
    });

    // afterEach functie wordt aangeroepen na iedere test.
    afterEach(function () {
        console.log('afterEach')
        // reset things we changed for testing
    });

    
    it('creates a user on valid registration', (done) => {

        const user = {
            email: "SuperSecure@gmail.com",
            password: "VerrySecure",
            firstname: "Mc",
            lastname: "Afee",
            residence: "test"
        };

        chai.request(server)
            .post('/api/user/register')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            });

    })

    //2nd Account
    it('creates a user on valid registration acc2', (done) => {

        const user = {
            email: "SuperSecure2@gmail.com",
            password: "VerrySecure",
            firstname: "Mc",
            lastname: "Afee",
            residence: "test"
        };

        chai.request(server)
            .post('/api/user/register')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            });

    })

    it('returns an error on invalid registration attempt', (done) => {

        const user = {
            email: "SuperSecure@gmail.com",
            firstname: "Mc",
            lastname: "Afee"
        };

        chai.request(server)
            .post('/api/user/register')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(500);
                done();
            });
    })

    it('returns an error on invalid login attempt', (done) => {

        const user = {
            email: 'user@server.com',
            password: 'secret1',
        }

        bcrypt.hash(user.password, saltRounds, (err, hash) => {
            chai.request(server)
                .post('/api/user/login')
                .send(user)
                .end(function (err, res) {
                    res.should.have.status(401);
                    done();
                });
        });
    })

    it('returns a token on valid login', function (done) {
        bcrypt.hash(user.password, saltRounds, (err, hash) => {
            chai.request(server)
                .post('/api/user/login')
                .send(user)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('token').that.is.a('string');
                    module.exports = {
                        token: res.body.token
                    }
                    done();
                });
        });
    });

    it('returns a token on valid login account 2', function (done) {

        const user = {
            email: "SuperSecure2@gmail.com",
            password: "VerrySecure"
        }
        bcrypt.hash(user.password, saltRounds, (err, hash) => {
            chai.request(server)
                .post('/api/user/login')
                .send(user)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('token').that.is.a('string');
                    module.exports = {
                        token: res.body.token
                    }
                    done();
                });
        });
    });
});