const ApiError = require('../models/apierror.model');
const connectionPool = require('../config/mySql');
const Activity = require('../models/activity.model');
const Attendee = require('../models/attendee.model');

module.exports = {
    ///api/activities
    GetAllActivities(req, res, next) {
        console.log('atempting request');
        console.log(`getActivity - request made by ${req.userId}`);

        connectionPool.query("SELECT * FROM activities", function (err, rows, fields) {
            if (err) {
                console.log(err);
                next(new ApiError('Failed to retrieve data', 500));
            }

            var activities = [];

            //Fill array
            for (var i = 0; i < rows.length; i++) {
                const activity = new Activity(rows[i].ID, rows[i].Name, rows[i].Description, rows[i].StartDate, rows[i].EndDate, rows[i].MaxAttendees, rows[i].OwnerID);
                activities.push(activity);
            }

            res.status(200).json(activities).end();
        });
    },

    ///api/activities
    PostActivity(req, res, next) {
        console.log(` postActivity - request made by ${req.userId}`);

        if (req.body.name && req.body.description && req.body.startdatetime && req.body.enddatetime && req.body.maxAttendees) {
            const activity = new Activity(0, req.body.name, req.body.description, req.body.startdatetime, req.body.enddatetime, req.body.maxAttendees, req.userId);

            connectionPool.query("INSERT INTO activities (Name, Description, StartDate, EndDate, MaxAttendees, OwnerID ) VALUES (?, ?, ?, ?, ?, ?)",
                [activity.name, activity.description, activity.startdatetime, activity.enddatetime, activity.maxattendees, req.userId], function (err, rows, fields) {
                    if (err) {
                        console.log(err);
                        next(new ApiError('Failed to post data', 500));
                    }

                    activity.id = rows.insertId;

                    connectionPool.query("INSERT INTO attendees (ActivityId,  UserID) VALUES (?,?)", [activity.id, req.userId], function (err, rows, fields) {
                        if (err) {
                            console.log(err);
                            next(new ApiError('Failed to post data', 500));
                        }

                        res.status(200).json(activity).end();
                    });

                });
        } else {
            const err = new ApiError('Failed to add object', 422);
            console.log(err);
            next(err);
        }
    },

    ///api/activities/:id
    GetActivityById(req, res, next) {
        console.log(`getActivityById - request made by ${req.userId}`);
        const id = req.params.id;

        connectionPool.query("SELECT * FROM activities WHERE ID= ?", [id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                next(new ApiError('Failed to retrieve data', 500));
            }

            if (rows.length == 0) {
                return next(new ApiError('Object not found', 404));
            }

            const activity = new Activity(rows[0].ID, rows[0].Name, rows[0].Description, rows[0].StartDate, rows[0].EndDate, rows[0].MaxAttendees, rows[0].OwnerID);

            res.status(200).json(activity).end();
        });

    },

    ///api/activities/:id
    PutActivityById(req, res, next) {

        const requestingUser = req.userId;
        const id = req.params.id;
        console.log(`PutActivityById - request made by ${requestingUser}`);

        if (req.body.name && req.body.description && req.body.startdatetime && req.body.enddatetime && req.body.maxAttendees) {
            const activity = new Activity(0, req.body.name, req.body.description, req.body.startdatetime, req.body.enddatetime, req.body.maxAttendees, requestingUser);


            connectionPool.query("SELECT * FROM activities where ID = ?", [id], function (err, rows, fields) {

                //Object not found
                if (rows.length == 0) {
                    return next(new ApiError('Object not found', 404));
                }

                //Check if rightfull owner is modifiying entity.
                if (rows[0].OwnerID == activity.ownerId) {

                    connectionPool.query("UPDATE activities SET Name = ?, Description = ?, StartDate = ?, EndDate = ?, MaxAttendees = ? WHERE ID = ? AND OwnerID = ?",
                        [activity.name, activity.description, activity.startdatetime, activity.enddatetime, activity.maxattendees, id, activity.ownerId], function (err, rows, fields) {
                            if (err) {
                                console.log(err);
                                return next(new ApiError('Failed to replace entity', 500));
                            }

                            activity.id = id;

                            res.status(200).json(activity);
                        });
                }
                else {
                    return next(new ApiError('You are not the rightfull owner, not allowed to modify.', 401));
                }
            });
        } else {
            const err = new ApiError('Failed to add object', 400);
            console.log(err);
            next(err);
        }
    },


    ///api/activities/:id
    DeleteActivityById(req, res, next) {
        const requestingUser = req.userId;
        const id = req.params.id;
        console.log(`DeleteActivity - request made by ${requestingUser}`);

        connectionPool.query("SELECT * FROM activities where ID = ?", [id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return next(new ApiError('Fatal error occured', 500));
            }

            //Object not found
            if (rows.length == 0) {
                return next(new ApiError('Object not found', 404));
            }

            if (rows[0].OwnerID == requestingUser) {
                connectionPool.query("DELETE FROM activities where id = ?", [id], function (err, rows, fields) {
                    if (err) {
                        console.log(err);
                        return next(new ApiError('Fatal error occured', 500));
                    }

                    console.log('afected = ' + rows.affectedRows + ' res' + rows.affectedRows > 0);
                    if (rows.affectedRows > 0) {

                        //Remove cascading attendees attached
                        connectionPool.query("DELETE FROM attendees where ActivityId = ?", [id], function (err, rows, fields) {
                            if (err) {
                                console.log(err);
                                return next(new ApiError('Fatal error occured', 500));
                            }

                            res.status(200).json({ message: 'Succesfully removed' });
                        });
                    }
                    else {
                        return next(new ApiError('Object not found', 404));
                    }

                });
            }
            else {
                return next(new ApiError('You are not the rightfull owner, not allowed to modify.', 401));
            }
        });
    },

    //api/activities/:id/participants/
    PostAcivityParticipantById(req, res, next) {
        const requestingUser = req.userId;
        const id = req.params.id;

        console.log(`PostAcivityParticipantById - request made by ${requestingUser}`);

        //Check object existence first.
        connectionPool.query("SELECT * FROM activities WHERE ID = ?", [id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return next(new ApiError('Fatal error occured', 500));
            }

            //Object not found
            if (rows.length == 0) {
                return next(new ApiError('Object not found', 404));
            }

            //Check if already subscribed
            connectionPool.query("SELECT * FROM attendees WHERE ActivityId = ? AND UserID = ?", [id, requestingUser], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    return next(new ApiError('Fatal error occured', 500));
                }

                //Object not found
                if (rows.length == 0) {
                    //Not existing? Insert

                    connectionPool.query("SELECT activities.MaxAttendees,  count(*) as Current FROM activities JOIN attendees ON attendees.ActivityId = activities.ID JOIN users ON users.ID = attendees.UserID WHERE activities.ID = ?", [id], function (err, rows, fields) {
                        if (err) {
                            console.log(err);
                            return next(new ApiError('Fatal error occured', 500));
                        }

                        if(rows[0].Current + 1 > rows[0].MaxAttendees){
                           return next(new ApiError('Maxmimum number of attendees.', 412));
                        }

                        connectionPool.query("INSERT INTO attendees (ActivityId,  UserID) VALUES (?,?)", [id, requestingUser], function (err, rows, fields) {
                            if (err) {
                                console.log(err);
                                return next(new ApiError('Failed to post data', 500));
                            }
    
                            res.status(200).json({ Message: "Succesfully added user" }).end();
                        });
                    });
                }
                else {
                    res.status(200).json({ Message: "User already subscribed" }).end();
                }
            });
        });
    },

    //api/activities/:id/participants/
    GetAllActivityParticipantsById(req, res, next) {
        const requestingUser = req.userId;
        const id = req.params.id;

        console.log(`GetAllActivityParticipantsById - request made by ${requestingUser}`);

        connectionPool.query("SELECT users.ID, Firstname, Lastname, Email, Residence FROM activities JOIN attendees ON attendees.ActivityId = activities.ID JOIN users ON users.ID = attendees.UserID WHERE activities.ID = ?", [id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return next(new ApiError('Fatal error occured', 500));
            }

            //Object not found
            if (rows.length == 0) {
                return next(new ApiError('Object not found', 404));
            }

            var attendees = [];

            //Fill array
            for (var i = 0; i < rows.length; i++) {
                const attendee = new Attendee(rows[i].ID, rows[i].Firstname, rows[i].Lastname, rows[i].Email, rows[i].Residence);
                attendees.push(attendee);
            }


            res.status(200).json(attendees);
        });
    },

    //api/activities/:id/participants/:id
    GetActivityParticipantsById(req, res, next) {
        const id = req.params.id;
        const uId = req.params.uId;
        const requestingUser = req.userId;

        console.log(`GetActivityParticipantsById - request made by ${requestingUser}`);

        connectionPool.query("SELECT users.ID, Firstname, Lastname, Email, Residence FROM activities JOIN attendees ON attendees.ActivityId = activities.ID JOIN users ON users.ID = attendees.UserID WHERE activities.ID = ?", [id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return next(new ApiError('Fatal error occured', 500));
            }

            var attendees = [];

            //Fill array
            for (var i = 0; i < rows.length; i++) {
                const attendee = new Attendee(rows[i].ID, rows[i].Firstname, rows[i].Lastname, rows[i].Email, rows[i].Residence);
                attendees.push(attendee);
            }

            //Object not found
            if (!attendees[uId]) {
                return next(new ApiError('Object not found', 404));
            }

            res.status(200).json(attendees[uId]);
        });
    },

    //api/activities/:id/participants/:id
    DeleteActivityParticipantById(req, res, next) {
        const id = req.params.id;
        const uId = req.params.uId;
        const requestingUser = req.userId;

        console.log(requestingUser);
        
        //Check if JWT user matched the Requesting user to delete.
        if (uId != requestingUser){
            return next(new ApiError('Only the owner of an account is allowed to unsubscribe.', 401));
        }

        //Query activity relation
        connectionPool.query("SELECT users.ID FROM activities JOIN attendees ON attendees.ActivityId = activities.ID JOIN users ON users.ID = attendees.UserID WHERE activities.ID = ? AND users.ID = ?", [id, uId], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return next(new ApiError('Fatal error occured', 500));
            }

            //Object not found.
            if (rows.length == 0) {
                return next(new ApiError('Object not found', 404));
            } 

            connectionPool.query("DELETE FROM attendees where ActivityId = ? AND UserID = ?", [id, uId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    return next(new ApiError('Fatal error occured', 500));
                }

                if (rows.affectedRows > 0) {
                    res.status(200).json({ message: 'Succesfully removed' });
                }
                else {
                    return next(new ApiError('Object not found', 404));
                }
            });
        });
    },
}