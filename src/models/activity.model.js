const ApiError = require('./apierror.model');

class Activity {
    constructor(id, name, description, startdatetime, enddatetime, maxattendees, ownerId){
        this.id = id;
        this.name = name;
        this.description = description;
        this.startdatetime = startdatetime;
        this.enddatetime = enddatetime;
        this.maxattendees = maxattendees;
        this.ownerId = ownerId;
    }
}

module.exports = Activity;