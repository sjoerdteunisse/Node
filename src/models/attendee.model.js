class Attendee {
    constructor(id, firstname, lastname, email, residence){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.residence = residence;
    }
}

module.exports = Attendee;

