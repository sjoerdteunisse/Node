class User {
    constructor(fullName, email, residence, password){
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.residence = residence;
    }
}

module.exports = User;

