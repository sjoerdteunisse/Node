const router = require('express').Router();
const activitiesController = require('../controllers/activitiescontroller.js');
const VerifyToken = require('../config/verifyToken');


router.get('/activities', VerifyToken, activitiesController.GetAllActivities);
router.post('/activities', VerifyToken, activitiesController.PostActivity);
router.get('/activities/:id', VerifyToken, activitiesController.GetActivityById);
router.put('/activities/:id', VerifyToken, activitiesController.PutActivityById);
router.delete('/activities/:id', VerifyToken, activitiesController.DeleteActivityById);

router.post('/activities/:id/participants', VerifyToken, activitiesController.PostAcivityParticipantById);
router.get('/activities/:id/participants', VerifyToken, activitiesController.GetAllActivityParticipantsById);
router.get('/activities/:id/participants/:uId', VerifyToken, activitiesController.GetActivityParticipantsById);
router.delete('/activities/:id/participants/:uId', VerifyToken, activitiesController.DeleteActivityParticipantById);


module.exports = router;