DROP DATABASE IF EXISTS `nodedb`;
CREATE DATABASE `nodedb`;
USE `nodedb`;

CREATE USER 'nodeDB_user'@'%' IDENTIFIED BY 'secret';
CREATE USER 'nodeDB_user'@'localhost' IDENTIFIED BY 'secret';

GRANT ALL ON `nodedb`.* TO 'nodeDB_user'@'%';
GRANT ALL ON `nodedb`.* TO 'nodeDB_user'@'localhost';



CREATE TABLE `users` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(32) NOT NULL,
  `Lastname` varchar(64) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Residence` varchar(128) NOT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `LastupdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;


CREATE TABLE `attendees` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ActivityId` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  `LastupdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


CREATE TABLE `activities` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(64) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `MaxAttendees` int(10) NOT NULL,
  `OwnerID` int(10) NOT NULL,
  `LastupdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

